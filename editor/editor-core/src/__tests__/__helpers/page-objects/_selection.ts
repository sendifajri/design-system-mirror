import { akEditorSelectedNodeClassName } from '../../../styles';

export const selectionSelectors = {
  gapCursor: '.ProseMirror-gapcursor',
  selectedNode: `.${akEditorSelectedNodeClassName}`,
};
