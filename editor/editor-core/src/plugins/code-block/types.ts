export interface CodeBlockOptions {
  allowCopyToClipboard?: boolean;
}
