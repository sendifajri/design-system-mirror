export type DatePluginState = {
  showDatePickerAt: number | null;
};
