import React from 'react';

import Button from '../../src';

export default () => <Button appearance="warning">Warning button</Button>;
