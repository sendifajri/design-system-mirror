---
order: 2
---

import DoDont from '@atlaskit/gatsby-theme-brisk/src/components/do-dont';
import buttonConciseDo from './images/button-concise-do.png';
import buttonConciseDont from './images/button-concise-dont.png';
import buttonCapitalizationDo from './images/button-capitalization-do.png';
import buttonCapitalizationDont from './images/button-capitalization-dont.png';
import buttonActionDo from './images/button-action-do.png';
import buttonActionDont from './images/button-action-dont.png';

## Usage

Buttons are triggers for events or actions. They’re commonly used as part of larger components or patterns such as [forms](/patterns/forms) or [modal dialogs](/components/modal-dialog).

Buttons:

- move users through a sequence of screens
- act as calls to action (CTAs)
- with an icon to convey meaning quicker
- with a badge indicate a value

## Anatomy

![Button anatomy](images/button-anatomy.png)

1. **Icon:** Use an icon to convey more meaning.
2. **Label:** Text that indicates the result of selecting the button.

## Best practices

- Use a primary button to indicate the main action of a group button set.
- Subtle or secondary buttons should use a less dominant color.
- Button size matters - make sure the size of the button is large enough to interact with on web or other device but not too large so as to be visually overwhelming.
- Use action verbs or phrases to tell the user what will happen next.

## Alignment

- **Right align** the primary button to visually support navigation when using buttons to prompt a user to move through a sequence of screens (e.g. getting started guides).
- **Center align** buttons for [benefits modals](/patterns/benefits-modal) and other modals, with the primary button on the right.
- **Left align** buttons for single-page forms and focused tasks, and sort by importance from left to right.

## Content guidelines

Buttons labels should be concise and clear enough to indicate the next action to the user

<DoDont
  type="do"
  image={{
    url: buttonConciseDo,
    alt: 'Atlaskit button that says Reset Password'
  }}
>
  Use concise, easy to scan, and clear button labels to indicate the next action to the user.
</DoDont>

<DoDont
  type="dont"
  image={{
    url: buttonConciseDont,
    alt: 'Atlaskit button that says Click here to choose a password'
  }}
>
  Use long, redundant button labels. Drop unecessary articles, such as ‘a’ or ‘the’, for a more concise label.
</DoDont>

<DoDont
  type="do"
  image={{
    url: buttonCapitalizationDo,
    alt: 'Atlaskit button that says Copy ticket'
  }}
>
  Use sentence-case capitalization.
</DoDont>

<DoDont
  type="dont"
  image={{
    url: buttonCapitalizationDont,
    alt: 'Atlaskit buttons that say copy text in sentence case and all caps'
  }}
>
  Use title case captalization or all caps.
</DoDont>

<DoDont
  type="do"
  image={{
    url: buttonActionDo,
    alt: 'text that asks delete unpublished page? Followed by a delete CTA button and a cancel button'
  }}
>
  Use active verbs or phrases that clearly indicate action.
</DoDont>

<DoDont
  type="dont"
  image={{
    url: buttonActionDont,
    alt: 'text that asks delete unpublished page? Followed by a Yes CTA button and a No button'
  }}
>
  Use vague and generic labels that make the user read the dialog before taking action.
</DoDont>

## Accessibility

- Write alternative text for all button and links.

## Server

Consult [AUI](https://docs.atlassian.com/aui/latest/docs/buttons.html) for implementation details.

## Related

A button is commonly used in:

- [forms](/patterns/forms)
- [modal dialogs](/components/modal-dialog)
- [dropdown menus](/components/dropdown-menu)
