import React from 'react';

import { colors } from '@atlaskit/theme';

import { Skeleton } from '../../src';

export default function AvatarSkeletonWeightNormalExample() {
  return <Skeleton color={colors.Y500} weight="normal" />;
}
