export { default } from './Tooltip';
export {
  default as TooltipPrimitive,
  TooltipPrimitiveProps,
} from './TooltipPrimitive';
export { PositionType, TooltipProps } from './types';
