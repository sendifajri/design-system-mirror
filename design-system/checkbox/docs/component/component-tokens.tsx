import { Component } from 'react';

import { ComponentTokens } from '../../src/types';

export default class MockComponent extends Component<ComponentTokens> {}
