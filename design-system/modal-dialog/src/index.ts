export { default } from './components/ModalWrapper';
export { default as ModalTransition } from './components/ModalTransition';
export {
  Body as ModalBody,
  Header as ModalHeader,
  Footer as ModalFooter,
  Title as ModalTitle,
  BodyProps as BodyComponentProps,
  TitleTextProps as TitleComponentProps,
} from './styled/Content';
export {
  KeyboardOrMouseEvent,
  ActionProps,
  ScrollBehavior,
  ContainerComponentProps,
} from './types';
export { FooterComponentProps } from './components/Footer';
export { HeaderComponentProps } from './components/Header';
