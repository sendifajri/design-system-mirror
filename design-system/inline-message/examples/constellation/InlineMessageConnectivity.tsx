import React from 'react';

import InlineMessage from '../../src';

export default () => (
  <InlineMessage type="connectivity">
    <p>
      <a href="#">Log in</a> to see more information
    </p>
  </InlineMessage>
);
