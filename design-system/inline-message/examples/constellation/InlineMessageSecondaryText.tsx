import React from 'react';

import InlineMessage from '../../src';

export default () => (
  <InlineMessage title="Primary text" secondaryText="Secondary text">
    <p>Dialog</p>
  </InlineMessage>
);
