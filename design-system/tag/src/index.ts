export { default } from './Tag';

export { AppearanceType, TagColor } from './types';
